# TP 2 #

# Partie 1 : Linear and non-linear projection methods in Python #

```python
from sklearn.datasets.mldata import fetch_mldata

mnist = fetch_mldata('MNIST Original', data_home = custom_data_home)
```


*   Figure 1

![figure1](Figures/Figure_1.png)

On a ici une représentation sous forme de matrice 20x20 des chiffres choisie aléatoirement dans une base de dimension 64.

*   Figure 2

![figure2](Figures/Figure_2.png)

Nous avons ici une projection des points aléatoires des nombres.

*   Figure 3

![figure3](Figures/Figure_3.png)

Nous avons ici un ensemble principal des projections des nombres. On remarque que les points sont un peu près réunit par bloc mais pas totalement.

*   Figure 4

![figure4](Figures/Figure_4.png)

On voit déjà qu'il y a une meilleure représentation des points des nombres, en effet, il sont regroupé par une projection linéaire discriminatoire.

*   Figure 5

![figure5](Figures/Figure_5.png)

La projection isomap à réussi à regouper les points de nombres mais ce n'est pas la meilleur projection puisqu'il y a des petites quantités des nombres qui sont mélangées avec d'autres.


*   Figure 6 (Intégration linéaire des nombres locaux)

![figure6](Figures/Figure_6.png)

On a ici une intégration linéaire, la donnée est représenté sous forme de pates d'araignées mais les données en elle-même ne sont pas tous regroupé linéairement.

*   Figure 7 Modification linéaire locale des integrations des digits.

![figure7](Figures/Figure_7.png)

On remarque principalement que les données sont divisés en deux blocs
*   Le bloc de données du nombre 0 en rouge
*   Et tout le reste sous forme linaire a tendance verticale de la courbe avec une concentration des données accrue vers l'axe des x.

*   Figure 8 Intégration des digits Hessian local 

![figure8](Figures/Figure_8.png)

On remarque essentiellement que ce graphe est le graphe inversé sur l'axe des x de la figure 7.

*   Figure 9 : Alignement tangentielle de l'espace des digits.

![figure9](Figures/Figure_9.png)

Ce graphe est le même que celui de la figure 8 avec en paramètre local une alignement local de l'espace les digits tangentielle.

*   Figure 10 : Intégrations des digits MDS

![figure10](Figures/Figure_10.png)

On a ici un ensemble de nuages de points qui sont regroupé par nombre. On remarque cependant qu'il y a des nombre qui se trouve quand même dans d'autre ensemble de nuages de points.

*   Figure 11 : Intégration aléatoire d'un foret de digits.

![figure11](Figures/Figure_11.png)

La racine de ce graphe est 0, ses fils sont les nombres 4 et 5.

*   Figure 12 Intégration spectral des digits

![figure12](Figures/Figure_12.png)

*   Figure 13 Intégration des digits t-SNE

![figure13](Figures/Figure_13.png)

*   Figure 14 Intégration NCA des digits.

![figure14](Figures/Figure_14.png)


# Part 2 : Dimensionality reductionusing Knime #

